$(document).ready(function(){


    var url = window.location.href;
    var lastVal = url.split("#");
    if(lastVal[1] == 'course-it') {
        $('#course-it').show();
        $('#course-bussiness').hide();
        $('#course-mechnical').hide();
        $('#course-electrical').hide();
        $('#course-civil').hide();
    }
   else if(lastVal[1] == 'course-bussiness') {
        $('#course-bussiness').show();
       $('#course-it').hide();
       $('#course-mechnical').hide();
       $('#course-electrical').hide();
       $('#course-civil').hide();
   }
    else if(lastVal[1] == 'course-mechnical') {
        $('#course-mechnical').show();
       $('#course-it').hide();
       $('#course-bussiness').hide();
       $('#course-electrical').hide();
       $('#course-civil').hide();
   }
    else if(lastVal[1] == 'course-electrical') {
        $('#course-electrical').show();
       $('#course-it').hide();
       $('#course-bussiness').hide();
       $('#course-mechnical').hide();
       $('#course-civil').hide();
   }
    else if(lastVal[1] == 'course-civil') {
        $('#course-civil').show();
       $('#course-it').hide();
       $('#course-bussiness').hide();
       $('#course-mechnical').hide();
       $('#course-electrical').hide();
   }
    else{
        $('#course-it').show();
        $('#course-bussiness').hide();
        $('#course-mechnical').hide();
        $('#course-electrical').hide();
        $('#course-civil').hide();
    }
});
function courseit(){
    $('#course-it').show();
    $('#course-bussiness').hide();
    $('#course-mechnical').hide();
    $('#course-electrical').hide();
    $('#course-civil').hide();
}
function coursebussiness(){
    $('#course-it').hide();
    $('#course-bussiness').show();
    $('#course-mechnical').hide();
    $('#course-electrical').hide();
    $('#course-civil').hide();
}
function coursemechnical(){
    $('#course-it').hide();
    $('#course-bussiness').hide();
    $('#course-mechnical').show();
    $('#course-electrical').hide();
    $('#course-civil').hide();
}
function courseelectrical(){
    $('#course-it').hide();
    $('#course-bussiness').hide();
    $('#course-mechnical').hide();
    $('#course-electrical').show();
    $('#course-civil').hide();
}
function coursecivil(){
    $('#course-it').hide();
    $('#course-bussiness').hide();
    $('#course-mechnical').hide();
    $('#course-electrical').hide();
    $('#course-civil').show();
}
