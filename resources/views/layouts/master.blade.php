<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', 'webtrainings4u') - Web Training Institute Ludhiana Punjab , Website Designing, Software Development</title>
    	<link href="{{ asset('assets/images/logo-2.png') }}" rel="icon" />
    	<meta charset="UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="keywords" content="@yield('keywords')">
    	<meta name="description" content="@yield('description')">
    	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    	<link href="{{ asset('assets/css/jquery.bxslider.css') }}" rel="stylesheet">
    	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    	<script src="{{ asset('assets/js/angular.min.js') }}"></script>
	</head>
	<body>
	    <div class="topnav">
	        <div class="container">
	            <div class="top pull-right">
	                <ul>
	                    <li>
	                        <i class="fa fa-phone"></i>
	                        <span>+91 9915984427</span>
                        </li>
	                    <li>
	                        <script type="text/javascript" src="//www.skypeassets.com/i/scom/js/skype-uri.js
	                    "></script>
	                        <div id="SkypeButton_Call_webdevelopers3_1">
	                            <script type="text/javascript">
	                                Skype.ui({
	                                    "name": "call",
	                                    "element": "SkypeButton_Call_webdevelopers3_1",
	                                    "participants": ["webdevelopers3"],
	                                    "imageColor": "white",
                                        "imageSize": 24
                                    });
                                </script>
                            </div>
                        </li>
                        <li>
                            <a class="" style="text-decoration: none; cursor: pointer" data-target="#enquiry" data-toggle="modal">
                                <i class="fa fa-question-circle"></i>
                                <span>enquiry</span>
                            </a>
                        </li>
                        <li style="color:#fff">Currency</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal fade" id="enquiry" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" style="width: 400px;">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Quick Enquiry ( +91 9915984427 )</h4>
                        <span class=""> </span>
                    </div>
                    <div class="modal-body" >
                        <div data-ng-app="myApp" data-ng-controller="FormCtrl">
                            <input type="text" class="cnt" placeholder="Your Email" data-ng-model="form.email">
                            <input type="text" class="cnt" placeholder="Subject" data-ng-model="form.subject">
                            <textarea class="cnt msg" placeholder="Message" data-ng-model="form.msg"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default snd-btn pull-right" data-ng-click="submit()">SEND</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}"><img width="150" alt="logo" src="{{ asset('assets/images/logo2.png') }}"/></a>
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navheadercollapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                
                <div class="collapse navbar-collapse navheadercollapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index">Home</a></li>
    					<li><a href="index#course-it" onclick="courseit();">IT</a></li>
    					<li><a href="index#course-bussiness" onclick="coursebussiness();">Business</a></li>
    					<li><a href="index#course-mechnical" onclick="coursemechnical()">Mechnical</a></li>
    					<li><a href="index#course-electrical" onclick="courseelectrical()">Electrical</a></li>
    					<li><a href="index#course-civil" onclick="coursecivil()">Civil</a></li>
    					<li class="dropdown ">
    					    <a href="index#course-civil" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Language</a>
					    </li>
    					<li><a href="blog">Blog</a></li>
    					<li><a href="contact_us">Contact us</a></li>
    					<li>
    						<div class="dropdown">
    						    <button type="button" class="btn btn-default navbar-btn my-login" data-toggle="dropdown">Login</button>
    						    <div class="dropdown-menu" style="padding: 10px; background: #ddd">
    						        <form action="#" role="form">
    						            <div class="form-group">
    						                <label for="user">Email</label>
    						                <input type="text" class="form-control" id="user" placeholder="Email" />
    						                <label for="password">Password</label>
    						                <input type="password" class="form-control" id="password" placeholder="Password" />
        								</div>
        								<button type="submit" class="btn btn-default">Sign in</button>
    								</form>
    							</div>
    						</div>
        				</li>
		    		</ul>
			    </div>
    		</div>
	    </div>
        <script>
            function converCurrencyNav(currency){
                $.post('currencyConverter.php',{'currency':currency},function(response){
                    if(response) {
                        window.location.reload(function(){
        					console.log(response);
        				});
        			}
        		});
        	}
    	</script>
    	<div class="header">
    	    <div class="container">
    	        <div class="main-title">
    	            <h1><b>Digital Explora</b></h1>
    	            <h2>Better & Effective</h2>
    	            <h5>Learning by Industry Experts</h5>
    	            <a class="btn btn-primary main-btn" href="#courses">Lets Get Started</a>
	            </div>
            </div>
        </div>
        <div class="features-master">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="features">
                            <i class="fa fa-desktop"></i>
                            <h3>Online Classes</h3>
                            <p>Our online classes offer you the opportunity to learn on your own time and at your own pace. All of our classes are completely free and will earn you accredited ...</p>
                        </div>
                    </div>
                    <div class="col-md-3">
    					<div class="features">
    						<i class="fa fa-headphones"></i>
    						<h3>24/7 Access</h3>
    						<p>Welcome to the homepage for the Web Training'z 24 hour Pre-service ... see if your field observation course qualifies as a substitute for the 24-hour Preservice ...</p>
    					</div>
    				</div>
    				<div class="col-md-3">
    					<div class="features">
    						<i class="fa fa-laptop"></i>
    						<h3>Live Projects</h3>
    						<p>Service Provider of Training, PHP & MySQL Training, Web Designing, Live Project Training, Java Training & Services Offered By Web Training'z ...</p>
    					</div>
    				</div>
    				<div class="col-md-3">
    					<div class="features">
    						<i class="fa fa-users"></i>
    						<h3>Our Tutors</h3>
    						<p>Our tutors are real people that you cancount on for expert help 24/7. You can get a tutor online for help in more than 40
    							courses.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="courses-master" id="course-it">
		    <a name="course-it"></a>
		    <div class="container">
		        <div class="courses-heading">
		            <h1 class="text-center">Explore features to help you learn powerful new skills</h1>
				</div>
				<div class="row">
				    <a href="html-training">
				        <div class="col-md-3">
				            <div data-link="html-training" class="courses hover-nth-1">
				                <div class="icon">
				                    <img src="images/html.png" myModalLabel="html" class="normal">
				                    <img  src="images/html-white.png" alt="html-white" class="highlight">
			                    </div>
		                        <h4 class="text-center">HTML</h4>
		                        <table class="table">
		                            <tr>
		                                <td class="pull-left">Duration</td>
		                                <td class="pull-right"><i class="fa fa-clock-o"></i>6 week</td>
	                                </tr>
	                                <tr>
                                        <td class="pull-left">Fee</td>
                                        <td class="pull-right"><i class="fa fa-inr"></i>3,000</td>
                                    </tr>
                                </table>
                                <p>Become an expert of dynamic and responsive websites with HTML, CSS and JavaScript<br><strong>Read more</strong></p>
                            </div>
                        </div>
                    </a>
				</div>
			</div>
		</div>

		<!-- portfolio start -->
		<div class="portfolio">
			<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12 text-center courses-heading">
			            <h1>Our Portfolio</h1>
		            </div>
	            </div>
				<div class="row">
				    <div class="col-md-12">
				        <ul class="bxslider">
				            <li>
				                <img alt="1" src="images/1.jpg" />
			                    <span>
			                        <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary main-btn">View Project</a>
		                        </span>
	                        </li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title" id="myModalLabel1">
		                    S.T.E.P-Science &amp; Technology Entrepreneurs' Park
	                    </h4>
                    </div>
                    <div class="modal-body">
                        STEP (Science and Technology Entrepreneurs' Park) GNDEC Unique Institution setup to promote Entrepreneurship among the Science &amp; Technology persons.<br><br>
                        <img alt="step-gndec" src="images/step-gndec.jpg" class="img-responsive" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a class="btn btn-primary" href="http://stepgndec.com/" target="_blank">View Website</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
		</div>
		
		<div class="container Experts">
		    <div class="row">
				<div class="col-md-6">
				    <h3>Our Placements</h3>
				    <div class="row">
				        <div class="col-sm-3">
				            <img src="images/davinder.png" alt="User" class="img-responsive" />
			            </div>
			            <div class="col-sm-9">
			                <h4>Harleen Kaur</h4>
			                <span>PHP Framework Developer in Digimantra</span>
			                <p>Developed a PHP Framework powered by Open Source and Industry Standard Frameworks,This focus on reducing the development time and maintaining the security along with powerful features in single box.</p>
		                </div>
	                </div>
				</div>
				
				<div class="col-md-6 Testimonial">
				    <h3>Testimonial</h3><br>
				    <div class="row">
						<div class="col-sm-2">
							<img src="images/odesk1.jpg" alt="User" class="img-responsive img-circle"/>
						</div>
						<div class="col-sm-10">
							<p>"exceeded my expectations for this project. Fast delivery! Highly recommended. 5+++++"</p>
							<h5>Jim Harris</h5>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-master">
		    <div class="container">
		        <div class="row">
		            <div class="col-sm-6 col-md-3">
		                <div class="footer">
		                    <ul>
		                        <li>
		                            <img alt="footer-logo" src="images/footer-logo.png" class="img-responsive footer-logo" />
	                            </li>
                            </ul>
                            <p>We are the fastest growing destination for online courses across the world. Discover and learn wide variety of online courses from the best instructors across the the world.</p>
                            <ul>
                                <li><img src="images/gateway.png" alt="gateway"  class="img-responsive" /></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="footer">
                            <ul style="margin-left: 80px;">
                                <li><strong>PAGES</strong></li>
                                <li><a href="about_us">About us</a></li>
                                <li><a href="contact_us">Contact Us</a></li>
                                <li><a href="testimonials">Testmonials</a></li>
                                <li><a href="sitemap">Sitemap</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="footer">
                            <ul>
                                <li><strong>CONTACT US</strong></li>
                                <li><i class="fa fa-phone"></i><span>+91 9915984427</span></li>
                                <li><i class="fa fa-skype"></i><span>webdevelopers3</span></li>
                                <li><i class="fa fa-paper-plane"></i><span>development.help24@gmail.com</span></li>
                            </ul>
                            <div class="social">
                                <span><strong style="color: #ff6e00">FOLLOW US</strong></span><br>
                                <a target=_blank href="https://www.facebook.com/webtrainings4u"><i class="fa fa-facebook"></i></a>
                                <a target=_blank href="https://twitter.com/webtrainings4u"><i class="fa fa-twitter"></i></a>
                                <a target=_blank href="https://plus.google.com/+Webtrainings4uLudhiana"><i class="fa fa-google-plus"></i></a>
                                <a target=_blank href="https://www.linkedin.com/profile/view?id=221191893"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-md-3">
                        <div class="footer">
                            <div class="newsletter">
                                <ul>
                                    <li style="color: #ff6e00;"><strong>Subscribe for Newsletter</strong></li>
                                </ul>
                            </div>
                            <input type="text" class="wt-tab-form" placeholder="you@email.com" data-ng-model="form.name" style="width: 67%;border-radius: 0px;">
                            <button type="submit" class="btn btn-default snd-btn" data-ng-click="submit()" style="margin-top: 0;border-radius: 0;padding: 8px;}">Subscribe</button>
                            <p style="color: #FFFFFF">Be up to date with the latest technologies. Sign up for the weekly newsletter. Read our full Privacy Policy for details.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <p class="text-center">Copyright &copy; 2015 WebTrainings4u All Rights Rserved.</p>
            </div>
        </div>
        <script src="{{ asset('assets/js/bootstrap.js') }}"></script>
		<script src="{{ asset('assets/js/jquery.bxslider.js') }}"></script>
		<script type="text/javascript">
		var slider = $('.bxslider').bxSlider({
		    minSlides: 1,
		    maxSlides: 4,
		    slideWidth: 282,
		    slideMargin: 10
	    });
	    $('.bx-wrapper ul li span').hide();
	    $('.bx-wrapper ul li').hover(function(){
	        $(this).children('span').stop().slideDown(200);
        });
        $('.bx-wrapper ul li').mouseleave(function(){
            $this = $(this);
            $($this).children('span').stop().hide(200,function(){
                $($this).children('span').removeAttr('style').css('display','none');
            });
        });
        </script>
        <script type="text/javascript">
        // speed in milliseconds
            var scrollSpeed = 50;
        
        // set the default position
            var current = 0;
        
        // set the direction
            var direction = 'h';
        
            function bgscroll() {
        
        // 1 pixel row at a time
                current -= 1;
        
        // move the background with backgrond-position css properties
                $('div.header').css("backgroundPosition", (direction == 'h') ? current + "px 0" : "0 " + current + "px");
        
            }
        
        //Calls the scrolling function repeatedly
            setInterval("bgscroll()", scrollSpeed);
        
        </script>
        <script>
            $(function () {
                $('a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            window.$zopim || (function (d, s) {
                var z = $zopim = function (c) {
                    z._.push(c)
                }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0];
                z.set = function (o) {
                    z.set.
                            _.push(o)
                };
                z._ = [];
                z.set._ = [];
                $.async = !0;
                $.setAttribute('charset', 'utf-8');
                $.src = '//v2.zopim.com/?2a5aEeB24QIvx7sn8mx59dQ6bObVYpyI';
                z.t = +new Date;
                $.
                        type = 'text/javascript';
                e.parentNode.insertBefore($, e)
            })(document, 'script');
        </script>
        <script>
            $(document).ready(function () {
                $("[data-link]").click(function () {
                    window.location.href = $(this).attr("data-link");
                    return false;
                });
            });
        </script>
        <script type="text/javascript">
            var app = angular.module('myApp', []);
            app.controller('FormCtrl', function ($scope, $http) {

                $scope.submit = function () {
                    var e = $scope.form.email;
                    var s = $scope.form.subject;
                    var m = $scope.form.msg;
                    console.log("posting data....");
                    formData = $scope.form;
                    $http.post('mail.php', {email: e, subject: s, msg: m}).success(function (data) {/*alert(data.status)*/
                    });
                };

            });
        </script>
    	<script>
        	(function(i,s,o,g,r,a,m){
        	    i['GoogleAnalyticsObject']=r;
        	    i[r] = i[r] || function(){
        	        (i[r].q=i[r].q||[]).push(arguments);
    	        },
    	        i[r].l = 1*new Date();
    	        a = s.createElement(o),
    	        m = s.getElementsByTagName(o)[0];
    	        a.async = 1;
    	        a.src = g;
    	        m.parentNode.insertBefore(a,m);
        	}) (window,document,'script','//www.google-analytics.com/analytics.js','ga');
        	ga('create', 'UA-57379194-1', 'auto');
        	ga('send', 'pageview');
    	</script>
        <script src="{{ asset('assets/js/custom.js') }}"></script>
    </body>
</html>